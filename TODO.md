* Non-QWERTY keyboards
* Have a dynamic flag to check whether we're runing in 1.5 or 1.8 mode
* Identify what changed in ZSWindow between 1.5 and 1.8
	There's a new RECT unknown_var_18_1 initialized for each window creation
		it is the original RECT Bounds, but Bounds is now scaled to  the resolution in ZSWindow::OnResolutionChange18
	TextHeight_18 is now embedded and initialized for each window creation
		it is scaled to the resolution in ZSWindow::OnResolutionChange18
* Identify what changed in ZSGraphicsSystem between 1.5 and 1.8
* Leverage Ghidra/BinDiff to find significant differences between 1.5 and 1.8
	* Annotate the functions with actual difference
* Journal has changed in 1.8! see JournalWin::JournalWin in detail
* There might a bug lurking in the new Journal 1024 vs 1048 entries, confirm with Ghidra
* int ZSWindow::LeftButtonDown(int x, int y) is different
* DeathWin::Command is different
* void ZSOptionWin::LoadSettings() "TEXTUREREDUCTION"
* Flags can contain pointers, They are serialized so that is a danger. This is caught but let's fixup the scripts to be sure
* The cursor sometimes 'moves' beyond its allowed box, making for a bad experience
* ZSWindow dtor should be virtual but derive destructors are buggy

ObjectWin and ZSMainDescribe seem unused either in 1.5 and 1.8

int Creature::CreateTexture() - 435FA0 - strange difference, seems to call Engine->Graphics() but none of it in our code:
* functions called are 436B80, 479660. they seem to be a getter and a setter to a +840h 32 bits object in ZSGraphicsSystem object. If I am right, neither exist (nor the +840h var) in our project
* new script (4) in 1.8 access both functions also, these are all the accesses available
* seems to be to avoid creating textures twice? I added them in the project

for reference:
436B80 -> ZSGraphicsSystem::GetUnknown_var_18()
479660 -> ZSGraphicsSystem::SetUnknown_var_18(long)
